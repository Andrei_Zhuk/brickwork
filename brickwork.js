/**
 * Created by Andrei on 31.03.2015.
 */

(function ($) {

    $.fn.brickwork = function (params) {
        this.each(function () {
            if (this.getAttribute('data-init') === 'ready' && this.getAttribute('data-init') != undefined)
                return;
            this.setAttribute('data-init', 'ready');
            return new BrickWork(this, params, this.dataset);
        });
    };

    var $window = $(window);

    // Plugin defaults options
    var defaultOptions = {
        column: 3, // ���������� �������
        selector: '> *', // ��������, � �������� ����� ������������� ������
        ignore: null, // ��������, ������� ���������� ������������ ��� �������
        gutter: 0.5, // ������� ������ �������� � ���������(%)
        showMethod: 'animation',
        animateSpeed: '0.2s',
        animateDelay: 150,
        
        responsive: {
            tColumn: 2,
            mColumn: 1,
            tWidth: 768,
            mWidth: 480,
            pWidth: 320
        },

        // ��������� ��������
        wrapBox: null, // id ���������� ����������

        message: {
            wait: '<img alt="" src="data:image/gif;base64,R0lGODlhKAAoAOMAAAQCBMTGxERCRGRiZNTS1BwaHFRSVGxubNza3P///wAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQJBQAJACwAAAAAKAAoAAAESTDJSau9OOvNu/9gKI5kaZ7UMaxs67IIWQB0bd91IOP8rY+znvAnMgiOyKQSSUA5n9CodEqtWq/YrHbL7Xq/4LB4TC6bz+j0NgIAIfkECQUACwAsAAAAACgAKACDBAIErK6s3NrcHBoc7O7sxMbEBAYEtLa03N7cHB4c9Pb0////AAAAAAAAAAAAAAAABGxwyUmrvTjrzbv/iCCOZDkqXzUYBsC67SuzRUoNQK7vvF7bElxvuPsBhUSi0YZM9papprMIDE6H0I/0Csh6tldvBzwVc8hO8+YQaLvfcDehSq/b7/i8fs/v+/+AgYKDhIWGh4iJiouMjY6PdxEAIfkECQUACQAsAAAAACgAKACDBAIExMbE5OLkHBoc3N7cFBIUzMrM9Pb0JCYk////AAAAAAAAAAAAAAAAAAAAAAAABHkQlUmrpSHpzXsfQCiOpJh5qFeULHmmcAK29BujK93a96fvvVTu5wqiZsQRz5gYJkNLI/IJiAadT2tvmmV2sEntjRv2csBEcYycNm/QPzWMHXdr4Dp5SkDo+/+Afgd2hIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnAkRACH5BAkFAA8ALAAAAAAoACgAgwQCBMTGxERCROTi5BwaHNze3GRiZBQSFNTS1AwKDMzKzERGRPT29CQmJGRmZP///wSOsJ10ZqU20/C6/yBIAGRpniUXruGBvqfKzs8I3zK9ujec66KeD8jiCWPEle1oCgwM0Kh0KjUySQHF1ZRYbgOIbYkixobL3TIgUBC43/A43Hr9JWtqe5LO1BO9dXcgfEd+QIB9gh+EQoY6AwWRkpOUkgyKmJmam5ydnp+goaKjpKWmp6ipqqusra6vsLE0EQAh+QQJBQASACwAAAAAKAAoAIQEAgSsrqzU1tT08vQcGhzExsQUEhTc3twMCgz8+vzU0tQEBgS0srTc2tz09vQkJiTMyszk4uT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFouBjIMZYkmZKFlLrvjBMAHRt3zUb77GB/zcdbyiZAY8FR0OwbDKfzqbvCCwoFlgEdoEAZLfdhcFIxVnLPzHaDFnfEGQ34CynqevzNh6Onyv6d3V0dXx4BXp1gXKDcoWCiHKKboxujosDAZmam5yaDFOCRERxk6JDoIumPBEHra6vsK4OqrS1tre4ubq7vL2+v8DBwsPExcbHyMnKy8zNzs/LIQAh+QQJBQANACwAAAAAKAAoAIMEAgTExsTk4uQcGhzU1tQUEhTU0tT09vQMCgzMyswkJiTc3tz8+vz///8AAAAAAAAEilAVVGalNtNA9P3eMABkaZ5lYKDsSbUwECRxi4w1quboy5u7X+kmTNGKAF8xWCQiZ8hkIcoUOpcrpFII1eKw0e2v2/xys83pE201j49pKvt35cKtarBWsOj7/4B+BweBhYENiImKi4yNjo+QkZKTlJWWl5iZmpucnZ6foKGio6SlpqeoqaqrrK2aEQAh+QQJBQAPACwAAAAAKAAoAIMEAgS8vrzc3tzU0tQcGhzExsT09vQUEhQMCgzEwsTk4uTU1tQkJiTMysz8+vz///8EnpAddGalNtOy9P0eQQBkaZ5lMaDsSbUwkKwxi4w1Oufoy5u7X+kmLAUWRZKvqEoCiMlgcSmUCqHFo5P6ayaxVdr04LT+wD/tl5z0TnFFM4/Lk+fQPPXY6b4qBICBgoOBBgaEiIQPi4yNjo+QkZKTlJWWl5iZmpucnZ6foKGimAYBpqeoqacJnWJXrVuwX7JTnYeJuAKju7y9vr/AwY0RACH5BAkFABIALAAAAAAoACgAhAQCBMTGxDw6POTi5CQmJNTW1BQSFNTS1GRiZPT29Nze3AwKDMzKzDQyNNza3BwaHGRmZPz6/P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWrIGEsxliSZkoGhXq+7vMAdG3fdXDg/E32QECAEewtZkWcLon7MW3LZ+0ozRGrAGc1WqVih9isIcyVerc7rFYKViPR4fWz3X2z093xF2+2z695ZHxPZ2yAZnpwagMKjY6PkI4JCZGVkRKYmZqbnJ2en6ChoqOkpaanqKmqoA4Nrq+wsbCqg0wLtHG4arpdvGa+hMC2wkm3qbXFxEXGqAMIz9DR0tGr1dbX2JshACH5BAkFAA8ALAAAAAAoACgAgwQCBMTGxOzu7NTW1BwaHPz6/BQSFNTS1Nze3AwKDMzKzPT29CQmJPz+/OTi5P///wS5kJlkZqU20zD0/R5BAGRpnmVwoOxJtTAQKHGbjDWq5ujLm7tf6SZM0YoAXzFYJCJnyKQhyhQ6lyukUgjV4rDR7a/b/HKzzekTbTWPj2kq+3flwq1qsNaB6Pv/gH4LC4GFgQ+IiYqLiw0Cj5CRkpGMlZVzPAmWm4mYOZqcm541oKGXYaaWozGlqYqrMK2uiLA2s4u1LLKzuT23r6i/tMHCvS7Cw1rID8Ymu67NQ8vRJM+p1EnTxL/YCREAIfkECQUAEQAsAAAAACgAKACEBAIEvL685OLk1NLU9PL0HBoc/Pr8FBIUxMbE3N7cDAoM7O7s1NbU9Pb0JCYk/P78zMrM////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABcmgcyjHWJJmSiKMer5uUQB0bd81MuD8TfZAAAIS7ClmRZwuifsxbctn7SjNEasAZzVapWKH2OwhzJV6tzusVgpWI9Hh9bPdfbPT3fEXb7bPr3lkfE9nbIBmenBqAgmNjo+Qjg0NkZWREZiZmpubDwufoKGioAScpqaDTAqnrJmpSautrK9FsbKocbentEG2upq8QL6/mMFGCAHJysvMygTGPIV/cYl3cX5MZYTV04uW3wkN0E3Em+M+5cC56cXr7Oc2w8TwU+ztaiEAIfkECQUAFAAsAAAAACgAKACEBAIExMbE7O7sREJE1NbUJCYkFBIU/Pr83N7c1NLUZGJkDAoMzMrM9Pb03NrcNDI0HBoc/P785OLkbG5s////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABcZgYSzGWJJmSgaEer4uBAF0bd91kOD8TfZAQIAR7C1mRZwuifsxbctn7SjNEasAZzVapWKH2KwhzJV6tzusVgpWI9Hh9bPdfbPT3fEXb7bPr3lkfE9nbIBmenBqEgiNjo+Qjg0NkZWRFJiZmpubEQKfoKGioZylpYNMC6armahJqqyrrkWwsadxAw+6u7y9uwwEcX5MZYSJhsKCccdzs0GFf8vKbmSHxtN12IjVcRMK3+Dh4uAEwWq2ps5Ateia6kbtnO88CyEAIfkECQUAEAAsAAAAACgAKACEBAIExMbE7O7s1NbUHBoc/Pr8FBIU1NLU9Pb03N7cDAoMzMrM9PL0JCYk/P785OLk////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABbVgYyjGWJJmSgaDer4uQQB0bd91cOD8TfZAQGAR7ClmRZwuifsxbctn7SjNEasAZzVapWKH2KwhzJV6tzusVgpWI9Hh9bPdfbPT3fEXb7bPr3lkfE9nbIBmenBqDwmNjo+QjggIkZWRApiZmpuaBQ6coJsMfkxlhImGcaRJpkxypYeEq0WtSa+sg66zQXSIgqpksa6oc7m2u0C1Rbe0wse/asSwwHtx0rjUW87L17TGy8g9vYQhACH5BAkFABMALAAAAAAoACgAhAQCBKyurNza3Ozu7BQSFMTGxPz6/AwKDOTi5PT29BwaHNTS1AQGBLS2tNze3PTy9Pz+/BweHNTW1P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXV4CSOZFlKxEGkq8q+qmLO8wLceK7jDO2Ptp0wd3AIJMckcqk8JoLDIUPBqB6qjAPAitUyCtDo7qAQD8FmIYOQ3qHbxDIc955v2XZAfU7O68Nwa357cH15hG2Ch4BthnaIaYqPjGkHDQGYmZqbmQOUZj0/Pp9iB6KjfqGnJqRRpqsmCQ6ztLW2tQ8Duru8vbsGEL7CvLl4k36Oc5Cgcsd5KoOtUs3K0kLQi6nUcMulxtXa0cjf3NY7U+LP5G3dUejZ6ul278522PVz9ODx8PPb7OZ0HAgBACH5BAkFABIALAAAAAAoACgAhAQCBMTGxOzu7ERCRNTW1BQSFPz6/GRiZNze3AwKDNTS1PTy9FRSVNza3BwaHPz+/GxubOTi5P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXJoCSOZFkSRVKkq8q+qmPOswLceK7jCe2Ptp0w1/v5gsNh0VhLJpfMEtKpg0aBVGECcuh6v2AvYZq9JRxlXYBcVqVz6zcPLQfE6wnGYM/v+/kKbFlWVxKCVIRXh06JUYtPhVJ1AI1Mj0qRJJdamVh4nSILCKOkpaalCwKqq6ytqwYPrrKtCwWTd3Jnt5s7unW4b26/vFV0csBpwsfERMZvyG22w5O+y9TS1njOadCD2M/Mc7vX49rlud/c4WbbZd2I6e7rlO1Z74whACH5BAkFAA0ALAAAAAAoACgAgwQCBMTGxOzu7NTW1BQSFPz6/Nze3AwKDNTS1PTy9BwaHPz+/OTi5P///wAAAAAAAASzsMlJax3kkLw1/5pijSMCnGiqogfpTuYqp+3rxvNc22We77wKzqcCBmFEmfHYGCZPy6PzGQ1Ok1XelZi1bX3d1/fHFD6LZcpYl0aeWW3JWhlvvuHx+SrsShj+gIGCgQkChoeIiYcFC4qOiQkEdycBekUKkwCVmQeYk5uTGpmgd52jljSSn6gsnnekb6KrnK5vsGeyr6xQtWe3VKq6tKecwba7AKazoca+yMrCzMShvU+/WBEAIfkECQUADwAsAAAAACgAKACDBAIErK6s3NrcFBIUxMbE7O7s/Pr8DAoM1NLUBAYEtLK0HBoc9PL0/P781NbU////BKzwyUmrvTjrzbv/IGU4AmmW6EkK4eQkcHIA8BHPtdxKCOD/wODvsHv0hEhgonhMJom7plO4jE6fzCuy2pJqfdDuN8gNeb9h81hZLATe8Lgcrija7/i8Xs8o+P+AgX8GDYKGggNrPwRnWgcLij6MkTSQkZORB4mXjVcJloqYipqUomufpZ1TpJyUqK2Zm6GqTq+zlKy3kbZrpmO5vbRJvGO+aLLBrqDJsanKzpkRACH5BAkFAA8ALAAAAAAoACgAgwQCBMTGxOzu7ERCRNze3Pz6/GRiZBwaHAwKDNTS1PTy9ERGROTi5Pz+/GRmZP///wSV8MlJq7046827/2AojmRpnihjrGzrtmQCzHRt00h873Y+yrygT0QYGI/IJBLFbDqf0Kh0Sq0+FIKsdsvVFhrdcPcQDAaA5RuCnL6d22o2nPae4+T2uh2w3s/0dn1+gHOCe4RwhnlogXhziG2Kj4yFjnCQaZKXlImWbZhlmp+ckZ5poEKmZag8oqekmapmsKGyPKw7CBEAIfkECQUACQAsAAAAACgAKACDBAIExMbE7O7s/Pr8HBocDAoM1NLU9PL0/P78////AAAAAAAAAAAAAAAAAAAAAAAABH4wyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPdG3f5CHsfO/zA8Rv+CMAjsikEhkwLJ/KghFKbVKp0ivUql1mu0ouGPkdH8Xmshk9Vo/ZYDcY3pV36Vq7Fn/VX/lYU2YAgFB+VU6DAIdbiYOMT4VPkEuSXoJrjmmYb5ptnHOecREAIfkECQUADgAsAAAAACgAKACDBAIExMbEPDo85OLkNDI09PL0HBoc1NLUZGJkDAoM7O7s/Pr83NrcZGZk////AAAABIPQyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPdG3fZKHsfO/zC5IBQCwaj8WAEMk8KkfDpvQpikqZ1JD16mQQvuCwOLzlJg9mZKKcDqDTxjXc6J4X5XZAPY+37+19c39zgXCDcIVtb4BsZodpiY6LhI1cAQwImZqbnJuVV1kgn1NLeaEfEQAh+QQJBQANACwAAAAAKAAoAIMEAgS8vrzc3tzs7uzU0tT09vQcGhzExsQMCgzEwsTk4uT08vT8+vz///8AAAAAAAAEgbDJSau9OOvNu/9gKI5kaZ5oqq5s675wLM90bd/kMux87/MMkgFALBqPxYQQyTwqR8Om9CmKSpnUkPXqXHKRWdD2Swx/xuREIcBuu9/tBPqbIJCPiDm3fjfm+0l2gAB/g3yDhYABBQKNjo+Qj3pXB16AZh6TU5Z9mB2aTZ4coFgkEQAh+QQJBQAGACwAAAAAKAAoAIIEAgTExsTs7uz8+vwcGhz08vT///8AAAADZ2i63P4wykmrvTjrzbv/YCiOZGmeaKqubOu+XCHMdG3TA0cAfO//vYAOSPwJN7ui8qhJKonMjPNpHFKBUcz0ystetlyvBXwVV8hUMwX9VE/YSys3KJ8D3BJ4ER/RQ+tzfBB+WIBhHAkAIfkECQUACQAsAAAAACgAKACDBAIExMbEREJENDI01NLUZGJkHBoc3NrcbG5s////AAAAAAAAAAAAAAAAAAAAAAAABF8wyUmrvTjrzbv/YCiOZGmeaKqubOu+cCzPdG3feK7vvCYMwKBwGCSQDICkcslUBo7NKPM5QkqvVJH1Gs2GttwpNNz0gsDkpPmDTq89bfK7Ew/POYiCfs/v7w89gYISEQAh+QQJBQAIACwAAAAAKAAoAIMEAgS8vrzc3tz08vQcGhzExsTk4uT09vT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAETBDJSau9OOvNu/9gKI5kaZ5oqq5s675wLM90bd94ru9879+FgHBILA4HJAJgyWw6mYXkc+qMjpTUrFWEzU63IYF4TC6TD7+0es3+RAAAOw==" />'
        }
    };

    function BrickWork(container, options, data) {
        this.container = container;
        this.options = $.extend(defaultOptions, options, data);
        this.items = $(container).find(this.options.selector).not(this.options.ignore);
        this.column = this.options.column;
        // �������� �������� �� �������, � �� �� �������� ����������� ������
        if (!this.items.length)
            return;
        this.parentSelector = this.items[0].parentNode;
        this.init();
    }


    BrickWork.prototype = {
        report: function (content, type) {
            if (type === 'info') {
                console.info('[BWork]:' + content);
            } else if (type === 'log') {
                console.log('[BWork]:' + content);
            }
        },

        init: function () {
            var self = this, resizetimer;
            self.report('Plug-in init', 'info');

            if (self.options.wrapBox && typeof self.options.wrapBox === 'string')
                self.createwrapBox();

            self
                    .createMessagesBox()
                    .showMbox(this.options.message.wait)
                    .preStyling();

            /**
             * Wait when all page is loaded
             */
            window.addEventListener('load', function () {
                var images = self.parentSelector.getElementsByTagName('IMG');
                var array = [];
                for (var i = 0; i < images.length; i++) {
                    if (images[i].complete) {
                        array.push(true);
                    } else {
                        array.push(false);
                    }
                }

                if (array.indexOf(false) === -1 && array != [])
                    self
                            .renderGrid()
                            .showGrid(self.options.showMethod);
            }, false);

            window.addEventListener('resize', function () {
                if (resizetimer) {
                    clearTimeout(resizetimer);
                }
                resizetimer = setTimeout(function () {
                    self.action_resize();
                }, 0);
            }, false);
        },

        /**
         * Create wrap box method.
         *
         */
        createwrapBox: function () {
            var wrapBox, self = this;

            if (this.items[0].tagName === 'LI') {
                wrapBox = document.createElement('UL');
            } else {
                wrapBox = document.createElement('DIV');
            }

            wrapBox.style.cssText = 'height: 0; overflow: hidden;';
            wrapBox.className = 'bwork-wrap-box';
            wrapBox.id = this.options.wrapBox;

            // WrapAll
            for (var i = 0; i < this.items.length; i++) {
                wrapBox.innerHTML = wrapBox.innerHTML + this.items[i].outerHTML;
                this.items[i].parentNode.replaceChild(wrapBox, this.items[i]);
            }
            // set new container
            self.container = wrapBox;
        },

        /**
         * Create messages box method.
         * @returns {BrickWork}
         */
        createMessagesBox: function () {
            this.messagesBox = document.createElement('DIV');
            this.messagesBox.style.cssText = 'clear:both';
            this.messagesBox.className = 'bwork-message-box';
            this.parentSelector.parentNode.insertBefore(this.messagesBox, this.parentSelector.nextSibling);
            return this;
        },

        /**
         * Show messages box method.
         * @param content
         * @returns {BrickWork}
         */
        showMbox: function (content) {
            if (content !== undefined)
                this.messagesBox.innerHTML = content;
            this.messagesBox.style.display = 'block';
            return this;
        },

        /**
         * Hide messages box method.
         * @returns {BrickWork}
         */
        hideMbox: function () {
            this.messagesBox.innerHTML = '';
            this.messagesBox.style.display = 'none';
            return this;
        },

        mQueries: function () {
            var width = $(window).width();
            var optResponsive = this.options.responsive;

            if (width > optResponsive.tWidth) {
                this.column = this.options.column;
            } else if (width < optResponsive.tWidth && width > optResponsive.mWidth) {
                this.column = optResponsive.tColumn;
                //this.column = Math.floor(this.options.column / 2);
            } else {
                this.column = optResponsive.mColumn;
            }
        },

        /**
         * Pre-styling container elements method.
         * @returns {BrickWork}
         */
        preStyling: function () {
            var items = this.items, gutter = this.options.gutter;

            this.itemWidth = 100 / this.column - gutter * 2;
            for (var i = 0; i < items.length; i++) {
                items[i].style.width = this.itemWidth + '%';
                items[i].style.margin = gutter + '%';
                items[i].style.display = 'block';
                items[i].style.opacity = 0;
                items[i].setAttribute('data-listid', i);
            }
            return this;
        },

        renderGrid: function () {
            this.getItemsMatrix();
            var items = this.items;

            for (var i = 0; i < items.length; i++) {
                items[i].style.position = 'absolute';
                items[i].style.left = this.matrix_horz[i] + '%';
                items[i].style.top = this.matrix_vert[i] + 'px';
            }
            this.report('The plugin did his job. Next animation works', 'info');
            return this;
        },

        getItemsMatrix: function () {
            this.getColumnGrid();
            var columnPointer = [], matrix_horz = [], matrix_vert = [], minColumnHeight, indexOf;
            // �������� �� ������� ��������
            for (var i = 0; i < this.items.length; i++) {
                if (i < this.column) {
                    // ���� ������� ��������� � ������ ����, ������
                    // ����������� 0 �� ��������� � ��������������� ������� �� �����������
                    // ���������� ������ � ������ ���������
                    matrix_vert.push(0);
                    matrix_horz.push(this.columnGrid[i]);
                    columnPointer.push($(this.items[i]).outerHeight(true));
                } else {
                    // ���� ��� �� ������ ���, ��
                    // ������� ���������� ������ � ������� ���������
                    // ������ ������
                    // ��������� � ������ ������� �� ����������� � �� ���������
                    // �������� ����������� ������� �� �����
                    minColumnHeight = Math.min.apply(Math, columnPointer);
                    indexOf = columnPointer.indexOf(minColumnHeight);
                    matrix_vert.push(minColumnHeight);
                    matrix_horz.push(this.columnGrid[indexOf]);
                    columnPointer[indexOf] = $(this.items[i]).outerHeight(true) + columnPointer[indexOf];
                }
            }
            this.matrix_vert = matrix_vert;
            this.matrix_horz = matrix_horz;
            this.setContainerHeight(columnPointer);
        },

        setContainerHeight: function (columnGrid) {
            $(this.parentSelector).height(Math.max.apply(Math, columnGrid));
            //this.$container.height(Math.max.apply(Math, columnGrid));
        },

        getColumnGrid: function () {
            var columnGrid = [], columnWidth = this.itemWidth + this.options.gutter * 2;

            for (var i = 0; i < this.column; i++) {
                columnGrid.push(+(columnWidth * i).toFixed(2));
            }
            this.columnGrid = columnGrid;
        },

        showGrid: function (method) {
            this.hideMbox();
            var self = this;
            var counter = 0, delay = this.options.animateDelay, items = this.items;

            switch (method) {
                case 'static':
                    for (var i = 0; i < items.length; i++) {
                        items[i].style.opacity = '1';
                        counter++;
                    }
                    break;
                case 'animation':
                    this.setTransition();
                    for (var i = 0; i < items.length; i++) {
                        setTimeout(
                                (function (N) {
                                    return function () {
                                        items[N].style.opacity = '1';
                                    }
                                })(i), delay);
                        delay += this.options.animateDelay;
                        counter++;
                    }
                    break;
            }
        },

        setTransition: function () {
            var items = this.items;
            for (var i = 0; i < items.length; i++)
                items[i].style.transition = 'all ' + this.options.animateSpeed + ' ease';
        },

        delTransition: function () {
            console.log('del tran');
            var items = this.items;
            for (var i = 0; i < items.length; i++)
                items[i].style.transition = 'none 0 ease';
        },

        action_resize: function () {
            var self = this;

            self.mQueries();
            self.delTransition();
            self.preStyling();
            self.renderGrid();
            self.setTransition();
            self.showGrid(self.options.showMethod);
        }
    };

})(jQuery);

// Automatically find and run brickwall plugin
$(function () {
    if (document.getElementsByClassName('bw-init').length)
        $(document.getElementsByClassName('bw-init')).brickwork();
});
